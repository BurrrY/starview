#include <QString>

#ifndef G_KSP_RESSOURCE_H
#define G_KSP_RESSOURCE_H

class g_KSP_Ressource
{
public:
    g_KSP_Ressource();

    double getValue() ;
    void setValue(double value);

    double getMax() ;
    void setMax(double value);

    double getCurrent() ;
    void setCurrent(double value);

    QString getShortDesc() ;
    void setShortDesc(QString value);

    QString getDesc() ;
    void setDesc(QString &value);
private:
    double value;
    double max;
    double current;

    QString desc;
    QString shortDesc;
};

#endif // G_KSP_RESSOURCE_H
