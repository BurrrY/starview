#include "gen.h"

gen::gen()
{
}

QString gen::len(QString val, int l)
{
    while(val.length()<l) {
        val.append(" ");
    }

    return val;
}

QString gen::txt(const QString desc, const QVariant val, int l) {
    return len(desc + ": " + val.toString(), l);
}


QString gen::txt(const QString prefix, const QVariant val, const QString postfix,  int l) {
    return len(prefix + val.toString() + postfix, l);
}

QString gen::frmNbr(QVariant d) {
    return frmNbr(d.toDouble());
}

QString gen::frmNbr(double d) {
    if(abs(d)>1000000)
        return QString::number((d/1000000),'f', 1) + "M";
    else if(abs(d)>1000)
        return QString::number((d/1000),'f',1) + "k";
    else
        return QString::number(d,'f', 1);


    return  QString::number(d);
}



QString gen::resourceBar(QString label, double currentValue, double maxValue) {
    QString data = label + ":";

    data += gen::frmNbr(currentValue) + "/" + gen::frmNbr(maxValue) + " ";


    int len = data.length();

    int all = 40-len;
    double p = (double)all/maxValue;
    double curr  = currentValue * p;

    for(int i=0;i<curr;++i)
        data += "█";

    return data;
}

QString gen::resourceBar(QString label, double percent)
{
    QString data = label + ":";

    data += gen::frmNbr(percent) + "% ";


    int len = data.length();

    int all = 40-len;
    double p = (double)all/100;
    double curr  = percent * p;

    for(int i=0;i<curr;++i)
        data += "█";

    return data;
}
