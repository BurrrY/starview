#ifndef G_KSP_H
#define G_KSP_H

#include "g_ksp_ressource.h"
#include "svgame.h"



class g_KSP : public svGame
{
public:
    g_KSP();
    ~g_KSP();

    // svGame interface
public:
    void parseData(QVariantMap data);
    QUrl requestString() const;
    QList<QUrl> requestUrls() const;



    int state;
    bool RCS;
    bool Light;
    bool Gear;
    bool SAS;
    bool Break;

private:
    QString ressourceBar(g_KSP_Ressource *res, bool showValue);
    std::vector<g_KSP_Ressource*> resources;

    g_KSP_Ressource *solidFuel = new g_KSP_Ressource();
    g_KSP_Ressource *liquidFuel = new g_KSP_Ressource();
    g_KSP_Ressource *electricCharge = new g_KSP_Ressource();
    g_KSP_Ressource *MonoPropellant = new g_KSP_Ressource();
    g_KSP_Ressource *Oxidizer = new g_KSP_Ressource();
    g_KSP_Ressource *IntakeAir = new g_KSP_Ressource();
    g_KSP_Ressource *Ore = new g_KSP_Ressource();
};

#endif // G_KSP_H
