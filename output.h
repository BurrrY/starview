#ifndef OUTPUT_H
#define OUTPUT_H


#include "svgame.h"

#include <QtSerialPort/qserialport.h>

//#include <QtSerialPort/QSerialPort>

//#include <QtSerialPort/QtSerialPort>


class Output
{
public:
    Output(svGame *pGame, const QString p);
    ~Output();

    bool isNotWorking();
    void sendData();
private:
    svGame *game;
    bool isWorking;

    QSerialPort *port;
};

#endif // OUTPUT_H
