#ifndef JSONFETCHER_H
#define JSONFETCHER_H

#include "svgame.h"

#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>

#include <QScopedPointer>
#include <QNetworkReply>
#include <QJsonParseError>
#include <QJsonObject>


class jsonFetcher : public QObject
{
Q_OBJECT
public:
    jsonFetcher();
    jsonFetcher(QObject *parent) : QObject(parent) {}
    void fetchData();
    void setGame(svGame *g);
    void setHost(QString h);
private:
    QNetworkAccessManager* m_manager;
    svGame *game;
    QString host;
private slots:
    void dataReady(QNetworkReply *);

};

#endif // JSONFETCHER_H
