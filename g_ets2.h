#ifndef G_ETS2_H
#define G_ETS2_H

#include "svgame.h"



class g_ets2 : public svGame
{
public:
    g_ets2();
    ~g_ets2();

    // svGame interface
public:
    void parseData(QVariantMap data);
    QUrl requestString() const;
    QList<QUrl> requestUrls() const;
};
#endif // G_ETS2_H
