#ifndef GAMESELECT_H
#define GAMESELECT_H

#include "svgame.h"

#include <QDialog>

namespace Ui {
class gameSelect;
}

class gameSelect : public QDialog
{
    Q_OBJECT

public:
    explicit gameSelect(QWidget *parent = 0);
    ~gameSelect();

private:
    Ui::gameSelect *ui;
    gen sg;
private slots:
    void on_WTClick();
    void on_ETS2Click();
    void on_KSPClick();
};

#endif // GAMESELECT_H
