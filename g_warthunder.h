#ifndef G_WARTHUNDER_H
#define G_WARTHUNDER_H

#include "g_warthunder_mission.h"
#include "svgame.h"

class g_WarThunder : public svGame
{
public:
    g_WarThunder();

    // svGame interface
    void parseData(QVariantMap data);
    QUrl requestString() const;
    QList<QUrl> requestUrls() const;

private:
    QList<g_WarThunder_mission> Missions;
};

#endif // G_WARTHUNDER_H
