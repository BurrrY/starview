#include "g_ksp_ressource.h"

g_KSP_Ressource::g_KSP_Ressource()
{
    max = -1;
    value = -1;
    desc = "-";
    shortDesc = "-";
    current = -1;
}
double g_KSP_Ressource::getValue()
{
    return value;
}

void g_KSP_Ressource::setValue(double value)
{
    value = value;
}
double g_KSP_Ressource::getMax()
{
    return max;
}

void g_KSP_Ressource::setMax(double value)
{
    max = value;
}
double g_KSP_Ressource::getCurrent()
{
    return current;
}

void g_KSP_Ressource::setCurrent(double value)
{
    current = value;
}
QString g_KSP_Ressource::getShortDesc()
{
    return shortDesc;
}

void g_KSP_Ressource::setShortDesc(QString value)
{
    shortDesc = value;
}
QString g_KSP_Ressource::getDesc()
{
    return desc;
}

void g_KSP_Ressource::setDesc( QString &value)
{
    desc = value;
}





