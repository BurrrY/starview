#include "g_ets2.h"

g_ets2::g_ets2()
{

    LED_Name[0] = "Blinker Links";
    LED_Name[1] = "Scheibenwischer";
    LED_Name[2] = "Handbremse";
    LED_Name[3] = "";
    LED_Name[4] = "";
    LED_Name[5] = "";
    LED_Name[6] = "";
    LED_Name[7] = "";
    LED_Name[8] = "";
    LED_Name[9] = "Blinker Rechts";


    for(int i=0;i<10;++i)
        LED[i] = false;
}


g_ets2::~g_ets2()
{

}



void g_ets2::parseData(QVariantMap data)
{
    int gear_current = data["gear"].toInt();


    double engineRpm = data["engineRpm"].toDouble();
    double engineRpmMax = data["engineRpmMax"].toDouble();

    double fuel = data["fuel"].toDouble();
    double fuelCapacity = data["fuelCapacity"].toDouble();

    double truckSpeed = data["truckSpeed"].toDouble();


    QString sourceCity = data["sourceCity"].toString();
    QString destinationCity = data["destinationCity"].toString();


    double connected = data["connected"].toBool();
    bool gamePaused = data["gamePaused"].toBool();

    QString fuelBar = gen::resourceBar("Tank", fuel, fuelCapacity);
    QString RPMBar = gen::resourceBar("RPM", engineRpm, engineRpmMax);

    if(!connected) {
        for(int i=1;i<8;++i)
            setLine("", i);

        setLine("No Connection", 4);
        return;
    }
    if(gamePaused){
        for(int i=1;i<8;++i)
            setLine("", i);

        setLine("Game paused", 4);
        return;
    }

    setLine(gen::txt("", gen::frmNbr(truckSpeed), "km/h", 12) + "G:" + gen::len(gen::frmNbr(gear_current), 8),0);
    setLine(sourceCity + " -> " + destinationCity, 2);
    //
    setLine(fuelBar, 3);


    //
    //
    //
    setLine(RPMBar, 7);
}

QUrl g_ets2::requestString() const
{
    return QUrl("http://localhost:25555/api/ets2/telemetry");
}


QList<QUrl> g_ets2::requestUrls() const
{
    QList<QUrl> res;
    res.append(QUrl("http://localhost:25555/api/ets2/telemetry"));
    return res;
}
