/**
* \file uart.h
* \brief  UART-Funtions
* $Author: buryflor $
* $Revision: 392 $
* $Date: 2014-07-12 15:08:05 +0200 (Sa, 12 Jul 2014) $
* $Id: uart.h 392 2014-07-12 13:08:05Z buryflor $
*/

//xMega_UART_V1.0.0
// http://florian-bury.de

#ifndef _UART_H_
#define _UART_H_

#include <avr/io.h>
#include <avr/interrupt.h>


#define bd_32_9600 207   /**<Baud-Rate 9600*/
#define bd_32_19200 103  /**<Baud-Rate 19200*/
#define bd_32_38400 51  /**<Baud-Rate 38400*/
#define bd_32_57600 34 /**<Baud-Rate 57600*/
#define bd_32_115200 17 /**<Baud-Rate 115200*/


extern volatile int USART_STR_CPL; 
uint8_t uart_str_count;  /**<Count Chars in Stiring */
extern void handleUartData(uint8_t);


/**
 * @fn void uart_init(USART_t *, int)
 * @brief Initialise UART for given Point
 * @param port Port for Communication
 * @param baud Speed for Port
 * @return void
 */
void uart_init(USART_t * port, int baud);


/**
 * @fn void uart_sendChar(USART_t * port, unsigned char c)
 * @brief Send a Single Char on given Port
 * @param port Port for Communication
 * @param c Char for send
 * @return void
 */
void uart_sendChar(USART_t * port, unsigned char c);


/**
 * @brief Send a String on given Port
 * @param port Port for Communication
 * @param data String to send
 * @return void
 */
void uart_string(USART_t *PORT, const char *data);


/**
 * @brief Handles an incoming char, writes data in Buffer
 * @param newChar Received Char
 * @param source Source-Port
 * @param buffer Buffer, which will be appendet
 * @return void
 */
void handleNewChar(char newChar, uint8_t source, uint8_t buffer[]);

#endif
