/**
* \file CONFIG.H
* \brief  Some Configuration for Board
* $Author: buryflor $
* $Revision: 298 $
* $Date: 2014-04-15 14:03:44 +0200 (Di, 15 Apr 2014) $
* $Id: config.h 298 2014-04-15 12:03:44Z buryflor $
*/
//xMega_Display_V1.0.0
// http://florian-bury.de



#ifndef __CONFIG__
#define __CONFIG__
#include <avr/io.h>

#ifndef F_CPU
	#define F_CPU 32000000UL
#endif


#define USART_MAXSTRLEN 450




#define USART_IN      USARTE0
#define USART_IN_SRC  2
#define USART_IN_INT  USARTE0_RXC_vect



//////////////////////////////////////
//LCD-CONNECTION
#define LCD_DATA_PORT       PORTA

#define LCD_CONF_PORT       PORTA
#define LCD_EN_B  2
#define LCD_EN_A  2
#define LCD_RS	0
#define LCD_RW	0

#define LCD_ENABLE_US 5

#endif
