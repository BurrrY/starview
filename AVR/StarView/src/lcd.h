/**
* \file lcd.h
* \brief  LCD-Routines
* $Author: buryflor $
* $Revision: 272 $
* $Date: 2014-04-01 17:38:01 +0200 (Di, 01 Apr 2014) $
* $Id: lcd.h 272 2014-04-01 15:38:01Z buryflor $
*/
//xMega_Display_V1.0.0
// http://florian-bury.de


#ifndef _LCD_H_
#define _LCD_H_


#include <avr/io.h>
#include <util/delay.h>
#include "config.h"


#define LCD_DDADR_LINE1         0x00
#define LCD_DDADR_LINE2         0x40
#define LCD_DDADR_LINE3         0x00
#define LCD_DDADR_LINE4         0x40


#define DISP_TIME_BOOT 15
#define DISP_TIME_RESET 5
#define DISP_TIME_ENTGL 20
#define DISP_SOFT_RESET  0x30




#define DISP_TIME_CLEAR_MS 3
#define DISP_TIME_HOME_MS 3

#define DISP_CMD_CLEAR 0x01
#define DISP_CMD_HOME 0x02

#define DISP_SET_FCTN 0x20
#define DISP_FCTN_BIT_8 0x10
#define DISP_FCTN_BIT_4 0x00

#define DISP_FCTN_LINE_1 0x00
#define DISP_FCTN_LINE_2 0x08

#define DISP_FCTN_DOTS_5x10 0x04
#define DISP_FCTN_DOTS_5x7 0x00

#define DISP_SET_DISP 0x08
#define DISP_DISP_ONOFF_ON 0x04
#define DISP_DISP_ONOFF_OFF 0x00
#define DISP_DISP_CURSOR_ON 0x02
#define DISP_DISP_CURSOR_OFF 0x00
#define DISP_DISP_BLINK_ON 0x01
#define DISP_DISP_BLINK_OFF 0x00


#define DISP_SET_ENTRY 0x04
#define DISP_ENTRY_CURSOR_INC 0x02
#define DISP_ENTRY_CURSOR_DEC 0x00
#define DISP_ENTRY_SHIFT_ON 0x01
#define DISP_ENTRY_SHIFT_OFF 0x00


void lcd_out( uint8_t data );
void lcd_char( uint8_t data );

void lcd_string( const char *data );
//void lcd_string_l( const char *data, uint8_t line );
//void lcd_string_lx( const char *data, uint8_t line, uint8_t X_Pos );

void lcd_enable( void );
void lcd_enableAll( void );
void lcd_out( uint8_t data );
void lcd_init(void);
void lcd_clear(void);
void lcd_clearAll(void);
void lcd_command( uint8_t data );
void lcd_setcursor( uint8_t x, uint8_t y );
void lcd_home( void );
void lcd_homeAll( void );
void lcd_nextLine(void);

#define LCD_Line_1 0x01
#define LCD_Line_2 0x02
#define LCD_Line_3 0x04
#define LCD_Line_4 0x08

#endif
