
#ifndef F_CPU
#define F_CPU 32000000UL
#endif

#include <asf.h>

#include "config.h"
#include "lcd.h"
#include "uart.h"

uint8_t uart_Buf[8*50]; /**<Buffer for DaisyChain-Output */
unsigned char  uart_SL032Cnt;  /**<Count chars for SL032-Buffer */
volatile int USART_STR_CPL = 0;  /**<Flag: String Recevie complete*/

void handleUartData(uint8_t);

void uartToLCD(void);
void int_init(void);

int main (void)
{
	//CLOCK INIT TO 32MHz
	OSC.CTRL |= OSC_RC32MEN_bm;
	while(!(OSC.STATUS & OSC_RC32MRDY_bm));
	CCP = CCP_IOREG_gc;
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc;
	//END CLOCK INIT

	board_init();
	
	

	_delay_ms(1);
	
	

	lcd_init();
	int_init();
	
	PORTE.DIRSET = (1<<3);	
	uart_init(&USART_IN, bd_32_115200);
	
	
	lcd_string("1234567");
	lcd_clear();
	

while(1){
	
	_delay_ms(1000);
}
	return 0;
}


void int_init(void)
{
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;	// Interrupts (Highlevel, Mediumlevel und Lowlevel freigeben)
	sei();																// Globale Interruptfreigabe
}


ISR(USART_IN_INT)
{
	handleNewChar(USART_IN.DATA, USART_IN_SRC, uart_Buf);
}

void uartToLCD(void)
{
	lcd_clearAll();
	lcd_string("12345678");
	lcd_setcursor(0,LCD_Line_1);
	int cnt = 0;
	char c = uart_Buf[0];
	while( c != '\r') {
		if(c=='\n')
			lcd_nextLine();
		else if(c=='#')		
			lcd_char(0xff);
		else
			lcd_char( c );
			
		++cnt;
		c = uart_Buf[cnt];
	}
}

void handleUartData(uint8_t source) {
	uint8_t offset = 0;
	switch(source) {		
		case USART_IN_SRC:
			uartToLCD();		
		break;
	}
	USART_STR_CPL = 0;
}