/**
* \file lcd.c
* \brief  LCD-Routines
* $Author: buryflor $
* $Revision: 258 $
* $Date: 2014-03-28 10:39:06 +0100 (Fr, 28 Mrz 2014) $
* $Id: lcd.c 258 2014-03-28 09:39:06Z buryflor $
*/
//xMega_Display_V1.0.0
// http://florian-bury.de

#include "lcd.h"
uint8_t currentLine=0;

void lcd_char( uint8_t data )
{
	LCD_CONF_PORT.OUTSET = (1<<LCD_RS);
	lcd_out( data );
	lcd_enable();	
	_delay_us( 10 );
} 

void lcd_clear( void )
{
	lcd_command(DISP_CMD_CLEAR);
	_delay_ms(1);
}

void lcd_home( void )
{
	lcd_command(DISP_CMD_HOME);
	_delay_ms(2);
}

void lcd_setcursor( uint8_t x, uint8_t y )
{
	uint8_t data;
	
	switch (y)
	{
		case LCD_Line_1:    // 1. Zeile
		data = 0x80 + LCD_DDADR_LINE1 + x;
		break;
		
		case LCD_Line_2:    // 2. Zeile
		data = 0x80 + LCD_DDADR_LINE2 + x;
		break;
		
		case LCD_Line_3:    // 3. Zeile
		data = 0x80 + LCD_DDADR_LINE3 + x;
		break;
		
		case LCD_Line_4:    // 4. Zeile
		data = 0x80 + LCD_DDADR_LINE4 + x;
		break;
		
		default:
		return;
	}
	
	lcd_command( data );
}

void lcd_command( uint8_t data )
{
	LCD_CONF_PORT.OUTCLR = (1<<LCD_RS);	
	lcd_out( data );	
	lcd_enable();
	_delay_us( 2 );
}
 
 void lcd_string( const char *data )
{
	int cnt = 0;
	while( *data != '\0' && cnt < 16) {
	lcd_char( *data++ );
	++cnt;
	}
}
 
void lcd_enable( void )
{
	LCD_CONF_PORT.OUTSET = (1<<LCD_EN_B);
	LCD_CONF_PORT.OUTSET = (1<<LCD_EN_A);
	_delay_ms( LCD_ENABLE_US);
	LCD_CONF_PORT.OUTCLR = (1<<LCD_EN_B);
	LCD_CONF_PORT.OUTSET = (1<<LCD_EN_A);
}
 
void lcd_out( uint8_t data )
{
	LCD_DATA_PORT.OUTSET = data;
}
 

void lcd_cmdAll(uint8_t data)
{
	lcd_out(data);
	lcd_enableAll();
}

void lcd_init(void) 
{
	LCD_DATA_PORT.DIR = 0xFF;
	LCD_DATA_PORT.OUT = 0x00;
	LCD_CONF_PORT.DIRSET = 1<<(LCD_EN_B|LCD_EN_A|LCD_RS|LCD_RW);
	LCD_CONF_PORT.OUTCLR = (1<<LCD_RW);
	
	_delay_ms(60);
	
	lcd_cmdAll(DISP_SOFT_RESET);
	_delay_ms(6);
	
	lcd_cmdAll(DISP_SOFT_RESET);
	_delay_ms(6);	
	
	lcd_cmdAll(DISP_SOFT_RESET);
	_delay_ms(6);
	
	lcd_cmdAll((DISP_SET_FCTN | DISP_FCTN_BIT_8 | DISP_FCTN_LINE_2 | DISP_FCTN_DOTS_5x10));
	_delay_ms(6);
	
	lcd_cmdAll((DISP_SET_DISP | DISP_DISP_ONOFF_ON | DISP_DISP_CURSOR_OFF | DISP_DISP_BLINK_OFF));
	_delay_ms(2);
	
	lcd_cmdAll((DISP_SET_ENTRY | DISP_ENTRY_CURSOR_INC | DISP_ENTRY_SHIFT_OFF));	
	_delay_ms(2);
}

void lcd_enableAll(void)
{	
	LCD_CONF_PORT.OUTSET = (1<<(LCD_EN_B|LCD_EN_A));
	_delay_ms( LCD_ENABLE_US);
	LCD_CONF_PORT.OUTCLR = (1<<(LCD_EN_B|LCD_EN_A));
}

void lcd_clearAll(void)
{
	lcd_cmdAll(DISP_CMD_CLEAR);
	
}

void lcd_homeAll(void)
{
	lcd_cmdAll(DISP_CMD_HOME);	
}

void lcd_nextLine(void)
{
	if(currentLine==LCD_Line_1) {
		currentLine = LCD_Line_2;	
	}
	else if(currentLine==LCD_Line_2) {
		currentLine = LCD_Line_2;
	}
	else if(currentLine==LCD_Line_3) {
		currentLine = LCD_Line_3;
	}
	else if(currentLine==LCD_Line_3) {
		currentLine = LCD_Line_1;
	}

	lcd_setcursor(0, currentLine);
	
}
