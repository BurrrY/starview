/**
* \file uart.c
* \brief  UART-Funtions
* $Author: buryflor $
* $Revision: 331 $
* $Date: 2014-05-21 16:09:45 +0200 (Mi, 21 Mai 2014) $
* $Id: uart.c 331 2014-05-21 14:09:45Z buryflor $
*/

//xMega_UART_V1.0.0
// http://florian-bury.de

#include "uart.h"
#include "config.h"
#include <avr/io.h>
#include <util/delay.h>


void uart_init(USART_t *PORT, int baudRate) {
	/* calculate Baudrate = (1/(16*(((I/O clock frequency)/Baudrate)-1)) = 12 */
	/* BSEL[11:0]-Wert bei 32MHz Takt und BSCALE[3:0]==0: */
	/* 207 : 9600 */
	/* 103 : 19200 */
	/* 51 : 38400 */
	/* 34 : 57600 */
	/* 16 : 115200 */																						
	PORT->BAUDCTRLA = 23;
	PORT->BAUDCTRLB = 164; //208;	//fehlertoleranz liegt so bei 0.01% statt 3.5%	


	PORT->CTRLB = USART_TXEN_bm | USART_RXEN_bm;
	PORT->CTRLC = USART_CHSIZE_8BIT_gc;
	PORT->CTRLA = USART_RXCINTLVL0_bm | USART_RXCINTLVL1_bm;
}


void uart_sendChar(USART_t *PORT, unsigned char c) {
   while (!( PORT->STATUS & USART_DREIF_bm));
   PORT->DATA = c;   
}


void uart_string(USART_t *PORT, const char *data)
{
	
	while( *data != '\0') {
		uart_sendChar(PORT, *data++ );
	}
}

void handleNewChar(char newChar, uint8_t source, uint8_t buffer[]) {
	if( USART_STR_CPL == 0 ) {	// wenn uart_string gerade in Verwendung, neues Zeichen verwerfen			
		// Daten werden erst in uart_string geschrieben, wenn nicht String-Ende/max Zeichenlänge erreicht ist/string gerade verarbeitet wird
		if( newChar != '\r' &&
			uart_str_count < USART_MAXSTRLEN ) {
			buffer[uart_str_count] = newChar;
			++uart_str_count;
		}
		else {
			buffer[uart_str_count] = '\r';
			buffer[uart_str_count+1] = '\0';
			uart_str_count = 0;
			USART_STR_CPL = 1;
			handleUartData(source);
		}
	}
		
}

