#include "svgame.h"

svGame::svGame()
{
    for(int i =0;i<8;++i)
        Line_Out.push_back("-");


    for(int i=0;i<10;++i)
        LED[i] = false;
}

QString svGame::getLine(int i) const
{
    return Line_Out[i];
}

void svGame::setLine(const QString &value, int i)
{
    if(i>9)
        return;

    Line_Out[i] = value.left(40);
}

int svGame::setMultiLine(const QString &value, int startRow, int maxRows)
{
    QString val = value;
    int cnt = 0;
    while(val.length()>0 && cnt < maxRows) {
        setLine(val, startRow++);
        ++cnt;
        val = val.mid(40);
    }

    return cnt;
}
