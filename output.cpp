#include "output.h"



Output::Output(svGame *pGame, const QString p)
{
    game = pGame;
    isWorking = false;

    port = new QSerialPort();
    port->setPortName(p);
    port->setBaudRate(QSerialPort::Baud115200);

    if (!port->open(QIODevice::ReadWrite)) {
        qDebug("Failed to open port:");
        isWorking = true;
    } else {
        qDebug("Serialport open:");
    }
}

Output::~Output()
{

}

bool Output::isNotWorking()
{
    return !isWorking;
}

void Output::sendData()
{
    isWorking = true;
    QString data = "";

    for(int i=0;i<4;++i)
        data += game->getLine(i) + "\n";

    data += "\r";
    QByteArray d = QByteArray::fromStdString(data.toStdString());
    port->write(d);
    qDebug(d);


    isWorking = false;
}

