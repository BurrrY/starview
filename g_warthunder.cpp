#include "g_warthunder.h"

g_WarThunder::g_WarThunder()
{
    LED_Name[0] = "Gears";
    LED_Name[1] = "Flaps";
    LED_Name[2] = "";
    LED_Name[3] = "";
    LED_Name[4] = "";
    LED_Name[5] = "";
    LED_Name[6] = "";
    LED_Name[7] = "";
    LED_Name[8] = "";
    LED_Name[9] = "";

}

void g_WarThunder::parseData(QVariantMap data)
{
    if(data.size()==1) //valid: false
    {
        bool state = data["valid"].toBool();

        if(!state) {
            for(int i=1;i<8;++i)
                setLine("Paused", 0);
        }

    }
    else if(data.size()==35) //Indicators
    {
        double rpm = data["rpm"].toDouble();
        double alt_m = data["altitude_10k"].toDouble()/3.2808;
        setLine(gen::txt("RPM:", gen::frmNbr(rpm), "", 12) + gen::txt("Alt:", gen::frmNbr(alt_m), "", 12) , 0);
    }
    else if(data.size()==27) //State
    {
        int throttle = data["throttle 1, %"].toInt();
        setLine(gen::resourceBar("THR", throttle), 3);
    }
    else //Mission.Info
    {
        Missions.clear();
        QList<QVariant> objectives = data["objectives"].toList();

        for(QVariant objective : objectives) {
            Missions.append(g_WarThunder_mission(objective.toMap()));
        }

        //Print Missions
        int row = 4;
        int cnt = 1;
        for(g_WarThunder_mission m : Missions) {
            if(m.Status == "in_progress")
            {
                QString mText = QString::number(cnt++) + ": ";
                if(m.Status == "in_progress")
                    mText += "-> " + m.Desc;
                else
                    mText += m.Desc;

                row += setMultiLine(mText, row,2);
            }
        }

    }

}

QUrl g_WarThunder::requestString() const
{
    return QUrl("http://localhost:8111/state");
    return QUrl("http://localhost:8111/indicators");
    ///hudmsg?lastEvt=0&lastDmg=0
    return QUrl("http://localhost:8111/mission.json");
}


QList<QUrl> g_WarThunder::requestUrls() const
{
    QList<QUrl> res;
    res.append(QUrl("http://localhost:8111/state"));
    res.append(QUrl("http://localhost:8111/indicators"));
    res.append(QUrl("http://localhost:8111/mission.json"));
    return res;
}
