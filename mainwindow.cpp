#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    gameSelector = new gameSelect();
    connect(gameSelector, SIGNAL(finished(int)), this, SLOT(onGameselectorClose(int)));

    connect(ui->sb_ReadDelay, SIGNAL(valueChanged(int)), this, SLOT(sb_ReadDelay_valueChanged(int)));
    connect(ui->pb_SaveSettings, SIGNAL(pressed()), this, SLOT(onSaveSettings()));


    writeTimer = new QTimer(this);
    connect(writeTimer, SIGNAL(timeout()), this, SLOT(writerUpdate()));

    readTimer = new QTimer(this);
    connect(readTimer, SIGNAL(timeout()), this, SLOT(updateReader()));

    settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, "FB_Software", "StarView");

    readTimer->setInterval(settings->value("timeInterval", 250).toInt());
    writeTimer->setInterval(settings->value("timeInterval", 250).toInt());
    ui->le_host->setText(settings->value("host", "http://localhost").toString());
    ui->sb_ReadDelay->setValue(settings->value("timeInterval", 250).toInt());
    ui->le_SerialPort->setText(settings->value("port", "---").toString());
    js.setHost(settings->value("host", "http://localhost").toString());


    gameSelector->exec();
}

void MainWindow::initGame() {
    ui->la_led_0->setText(game->LED_Name[0]);
    ui->la_led_1->setText(game->LED_Name[1]);
    ui->la_led_2->setText(game->LED_Name[2]);
    ui->la_led_3->setText(game->LED_Name[3]);
    ui->la_led_4->setText(game->LED_Name[4]);
    ui->la_led_5->setText(game->LED_Name[5]);
    ui->la_led_6->setText(game->LED_Name[6]);
    ui->la_led_7->setText(game->LED_Name[7]);
    ui->la_led_8->setText(game->LED_Name[8]);
    ui->la_led_9->setText(game->LED_Name[9]);
}

MainWindow::~MainWindow()
{
    delete game;
    delete o;

    delete ui;
}

void MainWindow::onGameselectorClose(int res) {
    bool gameSelected = false;
    if(res == sg.KSP) {
        qDebug() << "KSP";
        ksp = new g_KSP();
        game = ksp;
        gameSelected = true;
    }
    else if(res == sg.WT) {
        qDebug() << "WarThunder";
        wt = new g_WarThunder();
        game = wt;
        gameSelected = true;
    }
    else if(res == sg.ETS2) {
        qDebug() << "ETS 2";
        ets2 = new g_ets2();
        game = ets2;
        gameSelected = true;
    } else {
        qDebug() << "Unkown Game!";
    }

    if(gameSelected) {
        o = new Output(game, settings->value("port", 250).toString());
        js.setGame(game);
        initGame();
        readTimer->start(settings->value("timeInterval", 250).toInt());
        writeTimer->start(settings->value("timeInterval", 250).toInt());
    }
}

void MainWindow::onSaveSettings(void)
{
    settings->setValue("timeInterval", ui->sb_ReadDelay->value());
    settings->setValue("host", ui->le_host->text());
    settings->setValue("port", ui->le_SerialPort->text());

    readTimer->setInterval(settings->value("timeInterval", 250).toInt());
    writeTimer->setInterval(settings->value("timeInterval", 250).toInt());
    js.setHost(settings->value("host", "http://localhost").toString());
}

void MainWindow::sb_ReadDelay_valueChanged(int i) {
    readTimer->setInterval(i);
    writeTimer->setInterval(i);
}

void MainWindow::writerUpdate() {
   //LEDs
    updateProperty(ui->la_led_0, "ledActive", game->LED[0]);
    updateProperty(ui->la_led_1, "ledActive", game->LED[1]);
    updateProperty(ui->la_led_2, "ledActive", game->LED[2]);
    updateProperty(ui->la_led_3, "ledActive", game->LED[3]);
    updateProperty(ui->la_led_4, "ledActive", game->LED[4]);
    updateProperty(ui->la_led_5, "ledActive", game->LED[5]);
    updateProperty(ui->la_led_6, "ledActive", game->LED[6]);
    updateProperty(ui->la_led_7, "ledActive", game->LED[7]);
    updateProperty(ui->la_led_8, "ledActive", game->LED[8]);
    updateProperty(ui->la_led_9, "ledActive", game->LED[9]);

    //Lines
    ui->lineEdit_0->setText(game->getLine(0));
    ui->lineEdit_1->setText(game->getLine(1));
    ui->lineEdit_2->setText(game->getLine(2));
    ui->lineEdit_3->setText(game->getLine(3));

    ui->lineEdit_4->setText(game->getLine(4));
    ui->lineEdit_5->setText(game->getLine(5));
    ui->lineEdit_6->setText(game->getLine(6));
    ui->lineEdit_7->setText(game->getLine(7));

    if(o->isNotWorking()) {
     o->sendData();
    } else {
        qDebug() << "Sending Skipped!";
    }
}

void MainWindow::updateProperty(QWidget *obj, const char * prop, const QVariant value) {
   if(obj->property(prop) == value)
       return;

    obj->setProperty(prop, value);
    obj->style()->unpolish(obj);
    obj->style()->polish(obj);
    obj->update();
}

void MainWindow::updateReader() {
   js.fetchData();
}

void MainWindow::pb_start_clicked() {


}

