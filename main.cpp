#include "mainwindow.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile file(":/stylesheet.qss");
   if(file.open(QIODevice::ReadOnly | QIODevice::Text))
   {
       a.setStyleSheet(file.readAll());
       qDebug("Using StyleSheet");
       file.close();
   }else {
       qDebug("no StyleSheet");
   }


    MainWindow w;    

    w.show();
    return a.exec();
}
