#include "jsonfetcher.h"

jsonFetcher::jsonFetcher()
{

    m_manager = new QNetworkAccessManager();
    jsonFetcher *receiver = this;
    QObject::connect(m_manager, SIGNAL(finished(QNetworkReply*)), receiver, SLOT(dataReady(QNetworkReply*)));
}

void jsonFetcher::fetchData()
{
    for(QUrl url : game->requestUrls()) {
        url.setHost(host);
        m_manager->get(QNetworkRequest(url));
    }
}

void jsonFetcher::setGame(svGame *g)
{
    game = g;
}

void jsonFetcher::setHost(QString h)
{
    host = h;
}

void jsonFetcher::dataReady(QNetworkReply *pReply)
{
    if(pReply->error() != QNetworkReply::NoError) {
        qDebug() << "QNetworkReply-Error: " << pReply->error();
        return;

    }

    QByteArray data=pReply->readAll();
   if(data.size() == 0) {
       qDebug() << "No data from ";
       return;
   }

   QString webData(data);

   QJsonParseError jerror;
   QByteArray q;
   q.append(webData);

   QJsonDocument jdoc= QJsonDocument::fromJson(q,&jerror);
   if(jerror.error != QJsonParseError::NoError) {
       qDebug() << "Bad data!";
       return;
   }

   QJsonObject jsonObj = jdoc.object();

   //convert the json object to variantmap
   game->parseData(jsonObj.toVariantMap());
}

