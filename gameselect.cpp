#include "gameselect.h"
#include "ui_gameselect.h"

gameSelect::gameSelect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::gameSelect)
{
    ui->setupUi(this);
    connect(ui->pb_ETS2, SIGNAL(clicked()), this, SLOT(on_ETS2Click()));
    connect(ui->pb_WT, SIGNAL(clicked()), this, SLOT(on_WTClick()));
    connect(ui->pb_KSP, SIGNAL(clicked()), this, SLOT(on_KSPClick()));

}

gameSelect::~gameSelect()
{
    delete ui;
}

void gameSelect::on_WTClick()
{
    this->done(sg.WT);
}

void gameSelect::on_ETS2Click()
{
    this->done(sg.ETS2);
}

void gameSelect::on_KSPClick()
{
    this->done(sg.KSP);
}
