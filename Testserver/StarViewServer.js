var http = require('http');
var url = require('url');
var qs = require('querystring');
console.log("Starting StarView Test-Server!");
var fuelMax = randMin(1243212365,10);


var wtServer = http.createServer(function callback (request, response) {
	var urlData = url.parse(request.url, true);	
	var query = urlData.query;
	var result = "";
	var otherObject;
	var da;
	if(urlData.pathname == "/state") {
			da = "{\"valid\": true, " + 
					"\"aileron, %\": 0, " + 
					"\"elevator, %\": 43, " + 
					"\"rudder, %\": -6, " + 
					"\"flaps, %\": 0, " + 
					"\"gear, %\": 0, " + 
					"\"TAS, km/h\": 194, " + 
					"\"IAS, km/h\": 180, " + 
					"\"M\": 0.16, " + 
					"\"AoA, deg\": 7.9, " + 
					"\"AoS, deg\": -0.0, " + 
					"\"Ny\": 0.98, " + 
					"\"Vy, m/s\": -0.6, " + 
					"\"Wx, deg/s\": 0, " + 
					"\"throttle 1, %\": 11, " + 
					"\"RPM throttle 1, %\": 5, " + 
					"\"mixture 1, %\": 100, " + 
					"\"radiator 1, %\": 50, " + 
					"\"magneto 1\": 3, " + 
					"\"power 1, hp\": 0.0, " + 
					"\"RPM 1\": 814, " + 
					"\"manifold pressure 1, atm\": 0.91, " + 
					"\"water temp 1, C\": 13, " + 
					"\"oil temp 1, C\": 15, " + 
					"\"pitch 1, deg\": 39.9, " + 
					"\"thrust 1, kgs\": -32, " + 
					"\"efficiency 1, %\": 0}";
	}
	else if(urlData.pathname == "/indicators") {
		da = "{\"valid\": true,"+
				"\"speed\": 57.404869,"+
				"\"pedals1\": -0.064882,"+
				"\"pedals2\": -0.064882,"+
				"\"pedals3\": -0.064882,"+
				"\"stick_elevator\": 0.305795,"+
				"\"stick_ailerons\": 0.000275,"+
				"\"vario\": -4.499313,"+
				"\"altitude_hour\": 2569.715332,"+
				"\"altitude_min\": 569.715332,"+
				"\"altitude_10k\": 2569.715332,"+
				"\"aviahorizon_roll\": -0.119319,"+
				"\"aviahorizon_pitch\": -1.356408,"+
				"\"bank\": 0.201786,"+
				"\"turn\": 0.006935,"+
				"\"compass\": 268.048065,"+
				"\"compass1\": 268.048065,"+
				"\"clock_hour\": 7.516667,"+
				"\"clock_min\": 31.000000,"+
				"\"clock_sec\": 27.000000,"+
				"\"manifold_pressure\": 1.166848,"+
				"\"rpm\": 1168.298584,"+
				"\"oil_pressure\": 10.973817,"+
				"\"oil_temperature\": 10.973817,"+
				"\"water_temperature\": 10.680936,"+
				"\"mixture\": 1.000000,"+
				"\"fuel1\": 274.000000,"+
				"\"gears\": 0.000000,"+
				"\"gears_lamp\": 1.000000,"+
				"\"flaps\": 0.000000,"+
				"\"trimmer\": 0.326466,"+
				"\"throttle\": 0.121258,"+
				"\"weapon1\": 0.000000,"+
				"\"weapon2\": 0.000000,"+
				"\"prop_pitch\": 0.047653}";
	}
	else if(urlData.pathname == "/mission.json") {
		da  = "{"+
			   "\"objectives\" : ["+
				"  {\"primary\" : true,"+
				"	 \"status\" : \"in_progress\","+
				 " \"text\" : \"Protect the allied bombers while they're attacking the enemy battleship\"}],		"+	   
			   "\"status\" : \"running\""+
			"}";
	}
		
	response.writeHead(200, { 'Content-Type': 'application/json' });
	response.end(da);
	console.log(da);
});


var kspServer = http.createServer(function callback (request, response) {
	var urlData = url.parse(request.url, true);	
	var query = urlData.query;
	var result = "";
	var otherObject;
	var da;
	if(urlData.pathname == "/telemachus/datalink") {
		da  = "";
		fuelMax = randMin(234123231 , 10);
		for(var v in query) {
			var key = query[v];
			console.log(key + ": " + v);
			da += "\"" + v +"\"" + ":" + getValue(key) + ",";
		} 
	}
	
	da = "{" + da + "}";
	da = da.replace(",}", "}");
	
	response.writeHead(200, { 'Content-Type': 'application/json' });
	response.end(da);
	console.log(da);
});

var ets2Server = http.createServer(function callback (request, response) {
	var urlData = url.parse(request.url, true);	
	var query = urlData.query;
	var result = "";
	var otherObject;
	var da;
	if(urlData.pathname == "/api/ets2/telemetry") {
		da = "\"connected\":true," +
			"\"gameTime\":\"0001-01-05T09:03:00Z\"," + 
			"\"gamePaused\":false,"+
			"\"telemetryPluginVersion\":\"3\","+
			"\"gameVersion\":\"1.12\","+
			"\"trailerAttached\":true,"+
			"\"truckSpeed\":0.00535463,"+
			"\"accelerationX\":5.18050874E-05,"+
			"\"accelerationY\":0.0138828987,"+
			"\"accelerationZ\":-0.006118836,"+
			"\"coordinateX\":-16042.499,"+
			"\"coordinateY\":61.9985962,"+
			"\"coordinateZ\":4130.79834,"+
			"\"rotationX\":0.007891369,"+
			"\"rotationY\":-0.0009811945,"+
			"\"rotationZ\":4.32578645E-06,"+
			"\"gear\":1,"+
			"\"gears\":12,"+
			"\"gearRanges\":0,"+
			"\"gearRangeActive\":0,"+
			"\"engineRpm\":457.9689,"+
			"\"engineRpmMax\":2500.0,"+
			"\"fuel\":648.5042,"+
			"\"fuelCapacity\":1300.0,"+
			"\"fuelAverageConsumption\":0.873909354,"+
			"\"userSteer\":0.0,"+
			"\"userThrottle\":0.398506343,"+
			"\"userBrake\":1.0,"+
			"\"userClutch\":0.0,"+
			"\"gameSteer\":0.000818171829,"+
			"\"gameThrottle\":0.399011165,"+
			"\"gameBrake\":1.0,"+
			"\"gameClutch\":0.7634622,"+
			"\"truckMass\":0.0,"+
			"\"truckModelLength\":21,"+
			"\"truckModelOffset\":15360,"+
			"\"trailerMass\":8320.0,"+
			"\"trailerId\":\"digger1000\","+
			"\"trailerName\":\"Digger 1000\","+
			"\"hasJob\":true,"+
			"\"jobIncome\":392,"+
			"\"jobDeadlineTime\":\"0001-01-05T13:58:00Z\","+
			"\"jobRemainingTime\":\"0001-01-01T04:55:00Z\","+
			"\"sourceCity\":\"Luxembourg\","+
			"\"destinationCity\":\"Luxembourg\","+
			"\"sourceCompany\":\"Sanbuilders\","+
			"\"destinationCompany\":\"LkwLog GmbH\","+
			"\"retarderBrake\":0,"+
			"\"shifterSlot\":0,"+
			"\"shifterToggle\":0,"+
			"\"cruiseControlOn\":false,"+
			"\"wipersOn\":false,"+
			"\"parkBrakeOn\":false,"+
			"\"motorBrakeOn\":false,"+
			"\"electricOn\":false,"+
			"\"engineOn\":true,"+
			"\"blinkerLeftActive\":false,"+
			"\"blinkerRightActive\":false,"+
			"\"blinkerLeftOn\":false,"+
			"\"blinkerRightOn\":false,"+
			"\"lightsParkingOn\":false,"+
			"\"lightsBeamLowOn\":false,"+
			"\"lightsBeamHighOn\":true,"+
			"\"lightsAuxFrontOn\":false,"+
			"\"lightsAuxRoofOn\":false,"+
			"\"lightsBeaconOn\":false,"+
			"\"lightsBrakeOn\":true,"+
			"\"lightsReverseOn\":false,"+
			"\"batteryVoltageWarning\":false,"+
			"\"airPressureWarning\":false,"+
			"\"airPressureEmergency\":false,"+
			"\"adblueWarning\":false,"+
			"\"oilPressureWarning\":false,"+
			"\"waterTemperatureWarning\":false,"+
			"\"airPressure\":114.059341,"+
			"\"brakeTemperature\":46.11783,"+
			"\"fuelWarning\":false,"+
			"\"adblue\":47.42521,"+
			"\"adblueConsumpton\":0.0,"+
			"\"oilPressure\":37.0382538,"+
			"\"oilTemperature\":50.0049438,"+
			"\"waterTemperature\":28.21656,"+
			"\"batteryVoltage\":23.7188168,"+
			"\"lightsDashboard\":1.0,"+
			"\"wearEngine\":0.0,"+
			"\"wearTransmission\":0.0,"+
			"\"wearCabin\":0.0,"+
			"\"wearChassis\":0.0,"+
			"\"wearWheels\":0.0,"+
			"\"wearTrailer\":0.0,"+
			"\"truckOdometer\":" + randMin(234131 , 100); + ","+
			"\"cruiseControlSpeed\":0.0,"+
			"\"truckMake\":\"Iveco\","+
			"\"truckMakeId\":\"iveco\","+
			"\"truckModel\":\"Stralis\"";
	}
	
	
	da = "{" + da + "}";
	da = da.replace(",}", "}");
	
	response.writeHead(200, { 'Content-Type': 'application/json' });
	response.end(da);
	console.log(da);
});


wtServer.listen(8111);
kspServer.listen(8085);
ets2Server.listen(25555);



function randMin(d, min) {
	return ((Math.random()*d)+min);
}

function rand(d) {
	return (Math.floor(Math.random()*d))/1000;
}

function rBool() {
	return Math.floor(Math.random()*123) > 50;
}

function getValue(key) {
	if(key == "v.heightFromTerrain" || key == "v.surfaceVelocity" || key == "v.surfaceSpeed" || key == "v.altitude"  || key == "o.PeA"  || key == "o.ApA"  || key == "o.timeToAp"  || key == "o.timeToPe")
return rand(1343425225);
	else if (key == "v.rcsValue" || key == "v.lightValue" || key == "v.gearValue" || key == "v.brakeValue" || key == "v.sasValue")
return rBool();
	else if (key == "p.paused")
return 0; //randMin(3,0);
	else if (key == "v.name")
return "\"Vessel_Test-Name\"";
	else if (key.indexOf("r.resource") >= 0) 
	{
if (key.indexOf("r.resourceMax") >= 0)
	return fuelMax;
else if (key.indexOf("r.resourceCurrent") >= 0)
	return randMin(fuelMax, fuelMax/4);
else
	return randMin(fuelMax, fuelMax/4);
	}	
}

