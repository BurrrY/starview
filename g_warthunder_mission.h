#ifndef G_WARTHUNDER_MISSION_H
#define G_WARTHUNDER_MISSION_H

#include <QString>
#include <QVariantMap>



class g_WarThunder_mission
{
public:
    g_WarThunder_mission();
    g_WarThunder_mission(QVariantMap data);

    enum MissionState {in_progress, failed, won};

    bool isPrimary;
    QString Status;
    QString Desc;

};

#endif // G_WARTHUNDER_MISSION_H
