#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "g_ksp.h"
#include "g_ets2.h"
#include "output.h"
#include "svgame.h"
#include "gameSelect.h"
#include "g_warthunder.h"
#include "jsonfetcher.h"



#include <QMainWindow>
#include <QSettings>



namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void pb_start_clicked();

    void sb_ReadDelay_valueChanged(int);

    void writerUpdate();
    void updateReader();

    void onGameselectorClose(int res);
    void onSaveSettings(void);
private:
    svGame *game;
    Ui::MainWindow *ui;
    gameSelect *gameSelector;

    void readData(QString url);
    void displayData(QStringList data);
    void initGame();


    QString webData;
    gen sg;

    bool test = false;
    QTimer *readTimer;
    QTimer *writeTimer;
    void updateProperty(QWidget *obj, const char *prop, const QVariant value);

    jsonFetcher js;

    QSettings *settings;

    //game-pointer
    g_KSP *ksp;
    g_ets2 *ets2;
    g_WarThunder *wt;

    Output *o;
};

#endif // MAINWINDOW_H
