#include "g_ksp.h"

#include <QTime>

g_KSP::g_KSP()
{

    LED_Name[0] = "SAS";
    LED_Name[1] = "RCS";
    LED_Name[2] = "Light";
    LED_Name[3] = "Gear";
    LED_Name[4] = "Break";
    LED_Name[5] = "";
    LED_Name[6] = "";
    LED_Name[7] = "";
    LED_Name[8] = "";
    LED_Name[9] = "";

    liquidFuel->setShortDesc("LF");
    resources.push_back(liquidFuel);

    solidFuel->setShortDesc("SF");
    resources.push_back(solidFuel);

    electricCharge->setShortDesc("EL");
    resources.push_back(electricCharge);

    MonoPropellant->setShortDesc("MP");
    resources.push_back(MonoPropellant);

    Oxidizer->setShortDesc("OX");
    resources.push_back(Oxidizer);

    IntakeAir->setShortDesc("IA");
    resources.push_back(IntakeAir);

    Ore->setShortDesc("OR");
    resources.push_back(Ore);


}

g_KSP::~g_KSP()
{

}




void g_KSP::parseData(QVariantMap data)
{
    this->state = data["state"].toInt();
    this->RCS = data["rcs"].toBool();
    this->Light = data["light"].toBool();
    this->Gear = data["gear"].toBool();
    this->SAS = data["sas"].toBool();
    this->Break = data["break"].toBool();

    LED[0] = this->SAS;
    LED[1] = this->RCS;
    LED[2] = this->Light;
    LED[3] = this->Gear;
    LED[4] = this->Break;


    liquidFuel->setValue(data["lc"].toDouble());
    liquidFuel->setMax(data["lm"].toDouble());
    liquidFuel->setCurrent(data["l"].toDouble());

    solidFuel->setValue(data["sc"].toDouble());
    solidFuel->setMax(data["sm"].toDouble());
    solidFuel->setCurrent(data["s"].toDouble());

    electricCharge->setValue(data["ec"].toDouble());
    electricCharge->setMax(data["em"].toDouble());
    electricCharge->setCurrent(data["e"].toDouble());

    IntakeAir->setValue(data["ac"].toDouble());
    IntakeAir->setMax(data["am"].toDouble());
    IntakeAir->setCurrent(data["a"].toDouble());

    MonoPropellant->setValue(data["mc"].toDouble());
    MonoPropellant->setMax(data["mm"].toDouble());
    MonoPropellant->setCurrent(data["m"].toDouble());

    Oxidizer->setValue(data["oc"].toDouble());
    Oxidizer->setMax(data["om"].toDouble());
    Oxidizer->setCurrent(data["o"].toDouble());

    double Periapsis = data["pea"].toDouble();
    double Apoapsis = data["apa"].toDouble();

    QTime apa_time(0,0,0);
    int msecs = (int)(data["apat"].toDouble()*1000);
    apa_time=apa_time.addMSecs(msecs);
    QTime pea_time(0,0,0);
    msecs = (int)(data["peat"].toDouble()*1000);
    pea_time=pea_time.addMSecs(msecs);



    if(state != 0) {
        for(int i=1;i<8;++i)
            setLine("", i);
        switch(state) {
            case 1:
                setLine("State: Paused", 0);
            break;
            case 2:
                setLine("State: No Power", 0);
            break;
            case 3:
                setLine("State: Off", 0);
            break;
            case 4:
                setLine("State: Not found", 0);
            break;
        }

        return;
    }

    setLine(gen::txt("SurfSpeed", gen::frmNbr(data["surfSpeed"]), 19) + gen::txt("SurfVelo", gen::frmNbr(data["surfVelo"]), 1),0);
    setLine(gen::txt("V-Alt", gen::frmNbr(data["vAlt"]), 19) + gen::txt("T-Alt", gen::frmNbr(data["terrainHeight"]), 1),1);
    setLine(gen::txt("PeA", gen::frmNbr(Periapsis), 19) + gen::txt("ApA", gen::frmNbr(Apoapsis), 1),2);
    setLine(gen::txt("PeA-T", pea_time.toString(), 19) + gen::txt("ApA-T", apa_time.toString(), 1),3);

    int cnt=0;
    int pos = 0;
    while(pos < 4 && cnt < resources.size()) {
        if(resources.at(cnt)->getMax()>=0){
            setLine(ressourceBar(resources.at(cnt), true), pos+4);
            ++pos;
        }
        ++cnt;
    }

    for(; pos < 4;++pos)
        setLine("-", pos+4);


}


QString g_KSP::ressourceBar(g_KSP_Ressource *res, bool showValue) {
    QString data = res->getShortDesc() + ":";
    if(showValue)
        data += gen::frmNbr(res->getCurrent()) + "/" + gen::frmNbr(res->getMax()) + " ";


    int len = data.length();

    int all = 40-len;
    double p = (double)all/res->getMax();
    double curr  = res->getCurrent() * p;

    for(int i=0;i<curr;++i)
        data += "█";

    return data;
}

QUrl g_KSP::requestString() const
{
    return QUrl("http://localhost:8085/telemachus/datalink?state=p.paused&rcs=v.rcsValue&light=v.lightValue&gear=v.gearValue&break=v.brakeValue&sas=v.sasValue&vAlt=v.altitude&terrainHeight=v.heightFromTerrain&surfVelo=v.surfaceVelocity&surfSpeed=v.surfaceSpeed&vName=v.name&l=r.resource[LiquidFuel]&lc=r.resourceCurrent[LiquidFuel]&lm=r.resourceMax[LiquidFuel]&s=r.resource[SolidFuel]&sc=r.resourceCurrent[SolidFuel]&sm=r.resourceMax[SolidFuel]&e=r.resource[ElectricCharge]&ec=r.resourceCurrent[ElectricCharge]&em=r.resourceMax[ElectricCharge]&m=r.resource[MonoPropellant]&mc=r.resourceCurrent[MonoPropellant]&mm=r.resourceMax[MonoPropellant]&o=r.resource[Oxidizer]&oc=r.resourceCurrent[Oxidizer]&om=r.resourceMax[Oxidizer]&a=r.resource[IntakeAir]&ac=r.resourceCurrent[IntakeAir]&am=r.resourceMax[IntakeAir]&x=r.resource[XenonGas]&xc=r.resourceCurrent[XenonGas]&xm=r.resourceMax[XenonGas]&pea=o.PeA&apa=o.ApA&apat=o.timeToAp&peat=o.timeToPe");
}



QList<QUrl> g_KSP::requestUrls() const
{
    QList<QUrl> res;
    res.append(QUrl("http://localhost:8085/telemachus/datalink?state=p.paused&rcs=v.rcsValue&light=v.lightValue&gear=v.gearValue&break=v.brakeValue&sas=v.sasValue&vAlt=v.altitude&terrainHeight=v.heightFromTerrain&surfVelo=v.surfaceVelocity&surfSpeed=v.surfaceSpeed&vName=v.name&l=r.resource[LiquidFuel]&lc=r.resourceCurrent[LiquidFuel]&lm=r.resourceMax[LiquidFuel]&s=r.resource[SolidFuel]&sc=r.resourceCurrent[SolidFuel]&sm=r.resourceMax[SolidFuel]&e=r.resource[ElectricCharge]&ec=r.resourceCurrent[ElectricCharge]&em=r.resourceMax[ElectricCharge]&m=r.resource[MonoPropellant]&mc=r.resourceCurrent[MonoPropellant]&mm=r.resourceMax[MonoPropellant]&o=r.resource[Oxidizer]&oc=r.resourceCurrent[Oxidizer]&om=r.resourceMax[Oxidizer]&a=r.resource[IntakeAir]&ac=r.resourceCurrent[IntakeAir]&am=r.resourceMax[IntakeAir]&x=r.resource[XenonGas]&xc=r.resourceCurrent[XenonGas]&xm=r.resourceMax[XenonGas]&pea=o.PeA&apa=o.ApA&apat=o.timeToAp&peat=o.timeToPe"));
    return res;
}
