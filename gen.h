#include <QString>
#include <QVariant>

#ifndef GEN_H
#define GEN_H

class gen
{
public:
    gen();
    enum games {KSP, ETS2, WT};
    static QString len(QString val, int l);
    static QString txt(const QString desc, const QVariant val, int l);
    static QString frmNbr(double d);
    static QString frmNbr(QVariant d);
    static QString resourceBar(QString label, double currentValue, double maxValue);
    static QString resourceBar(QString label, double percent);
    static QString txt(const QString prefix, const QVariant val, const QString postfix, int l);
};

#endif // GEN_H
