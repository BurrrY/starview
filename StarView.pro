#-------------------------------------------------
#
# Project created by QtCreator 2015-04-30T21:45:52
#
#-------------------------------------------------

QT += core gui serialport
QT += network
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StarView
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    g_ksp.cpp \
    gen.cpp \
    g_ksp_ressource.cpp \
    svgame.cpp \
    output.cpp \
    g_ets2.cpp \
    gameselect.cpp \
    g_warthunder.cpp \
    jsonfetcher.cpp \
    g_warthunder_mission.cpp

HEADERS  += mainwindow.h \
    svgame.h \
    g_ksp.h \
    gen.h \
    g_ksp_ressource.h \
    output.h \
    g_ets2.h \
    gameselect.h \
    g_warthunder.h \
    jsonfetcher.h \
    g_warthunder_mission.h

FORMS    += mainwindow.ui \
    gameSelect.ui

OTHER_FILES += \
    stylesheet.qss

RESOURCES += \
    res.qrc
