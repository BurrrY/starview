#ifndef SVGAME_H
#define SVGAME_H

#include <QUrl>
#include <QVariantMap>
#include "gen.h"

class svGame
{
public:
    virtual void parseData(QVariantMap data) = 0;
    virtual QUrl requestString() const = 0;
    virtual QList<QUrl> requestUrls() const = 0;

    svGame();


    bool LED[10];
    QString LED_Name[10];
    QString title;
    QString getLine(int i) const;

protected:
    void setLine(const QString &value, int i);
    int setMultiLine(const QString &value, int startRow, int maxRows);

private slots:


private:
    std::vector<QString> Line_Out;
};




#endif // SVGAME_H

